#!/usr/bin/env python
# coding: utf-8
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import copy
import random

import uuid

import plotly.graph_objects as go

import dash
# import dash_daq as daq
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

import sys
sys.path.append(
    '/home/kino/work/00_HEA/fukushima_hea4_3features_visualization/python_module') 

from dash_periodictable_button import DashPtable
from df_trim import DfTrim
from HEA4_3f import Hea4_3Featuredata


random.seed(0)

g_key_list = ['total magnetic moment per volume',
            'TC(K)', 'resistivity(micro ohm cm)']
g_key_group_list = ['total_moment_group', 'TC_group', 'resistivity_group']
#g_key_display_list = [r'M (\mu_B /\AA^3)$', r'T_C (K)',
#                    r'R (\mu \Omega cm)']
g_key_display_list = [r'x = M (μB /Å^3)', r'y = TC (K)',
                    r'z = R (μΩ cm)']


def make_type_dict(key_list):
    dic = {}
    for key in key_list:
        dic[key] = float
    return dic

def make_operation_dict(key_list):
    dic = {}
    for key in key_list:
        dic[key] = np.abs
    return dic

g_key_type_list = make_type_dict(g_key_list)
g_key_operation_list = make_operation_dict(g_key_list)

g_hea4 = Hea4_3Featuredata("mt",
        type_conversion=g_key_type_list,
        post_conversion=g_key_operation_list)
g_df_all = g_hea4.select_stable(add_element_existence=True)

g_elements_exists = g_hea4.elements_exist(g_df_all)

g_activate_button_list = g_elements_exists

g_ndiv = 20
g_binmax = 5


g_trim = DfTrim(g_df_all, key=g_key_list, group=g_key_group_list, ndiv=g_ndiv, binmax=g_binmax)
g_df_trim = g_trim.df
print("data size",g_df_trim.shape)

def plot_go_3d(df: pd.DataFrame, key_list: list, key_display_list: list):
    """ plotly go 3d plot

    Args:
        df_trim (pd.DataFrame):  data
        key_list (list[3]): a list of key to plot
        key_display_list (list[3]): a list of axis labels
    """
    def make_xyz_rgb(df, key_list):
        """generate x,y,z and color

        Args:
            df (pd.DataFrame): data
            key_list (list): a list of keys

        Returns:
            np.array:  x
            np.array:  y
            np.array:  z
            np.array:  rgb
        """
        x = df[key_list[0]].astype(float).values
        y = df[key_list[1]].astype(float).values
        z = df[key_list[2]].astype(float).values

        sx = MinMaxScaler().fit_transform(x.reshape(-1, 1)).reshape(-1)
        sx = map(int, sx*255)
        sy = MinMaxScaler().fit_transform(y.reshape(-1, 1)).reshape(-1)
        sy = map(int, sy*255)
        sz = MinMaxScaler().fit_transform(z.reshape(-1, 1)).reshape(-1)
        sz = map(int, sz*255)

        rgb = []
        for sx1, sy1, sz1 in zip(sx, sy, sz):
            rgb.append("rgb({},{},{})".format(sx1, sy1, sz1))
        # print(rgb[:10])
        return x, y, z, rgb

    print("plot_go_3d: data size", df.shape)
    xt, yt, zt, rgbt = make_xyz_rgb(df, key_list)
    fig = go.Figure(data=[go.Scatter3d(
        x=xt,
        y=yt,
        z=zt,
        mode='markers',
        hovertext=df['material name'].values,
        marker=dict(
            size=2,
            color=rgbt,
            opacity=0.3
        )
    )]
    )
    fig.update_layout(
        width=800, height=800, 
        scene=dict(
        xaxis_title=key_display_list[0],
        yaxis_title=key_display_list[1],
        zaxis_title=key_display_list[2]))

    return fig 


def select_elements(df: pd.DataFrame, selection_type: str, selection_bool: bool, element_list: list) -> pd.DataFrame:

    debug = True
    
    columns = list(df.columns)
    
    # error check
    for elm in element_list:
        if elm not in columns:
            raise ValueError("element({}) not supported.".format(elm))
                
    # construct query sentence 
    _selection_type = " "+selection_type+" "
    element_true = []
    for elm in element_list:
        element_true.append("{} == {}".format(elm,selection_bool))
    query_sentence = _selection_type.lower().join(element_true)
    if debug:
        print("query_sentence '{}'".format(query_sentence))
    df_select = df.query(query_sentence)
    return df_select


g_ptable = DashPtable()

g_fig = plot_go_3d(g_df_trim, g_key_list, g_key_display_list)

app = dash.Dash(__name__)

def serve_layout():
    global g_fig
    session_id = str(uuid.uuid4())
    print("session_id", session_id)
    div_periodic_table = html.Div([
        #html.H2(children='periodic table'),
        # daq.ToggleSwitch(label='NOT', labelPosition='left',
        #     id='toggle-switch-not', value=False, style={'color': 'lightgrey', 'display': 'inline-block'}), 
        dcc.RadioItems(id='selection-type',
                        options=[{'label': i, 'value': i} for i in ['AND', 'OR']],
                        value='AND',
                        labelStyle={'display': 'inline-block'}),
        dcc.RadioItems(id='selection-bool',
                        options=[{'label': i, 'value': i} for i in ['True', 'False']],
                        value='True',
                        labelStyle={'display': 'inline-block'}),

        g_ptable.make_html_table(),
        ])
    div_ndiv = html.Div([
        #html.H2('data size control'),
        dcc.Slider(min=1, max=100,
            marks={i: str(i) for i in range(10,101,20)},
            value=20, id='ndiv'),
        html.Div(children='ndiv value:', id='ndiv-value', style={'margin-top': 10}),
        html.Div(children='', style={'margin-top': 20}),
        ])
    div_binmax = html.Div([
        dcc.Slider(min=1, max=100,
            marks={i: str(i) for i in range(10,101,20)}, 
            value=5, id='binmax'),
        html.Div(children='binmax value:', id='binmax-value', style={'margin-top': 10}),
        html.Div(children='', style={'margin-top': 20}),
                         ])
    div_data_size = html.Div([div_ndiv, div_binmax])

    return html.Div([
        html.Div(session_id, id='session-id', style={'display': 'none'}),
        html.Div(children='total size of data: {}'.format(g_df_all.shape[0]),
            id='selected-datasize', style={'margin-top': 10}),

        html.Table([
            html.Tr([
                html.Th(html.H2(children='element selection')),
                html.Th(html.H2(children='plot size selection'))
                ]),
            html.Tr([
                html.Td([div_periodic_table], style={'padding': '5px', 'border': 'solid 3px'}),
                html.Td([div_data_size], style={'padding': '5px', 'border': 'solid 3px'})
                ])
            ]),

        html.Button('start-plot', id='start-plot'),
        html.Div(-1, id='start-plot-hidden', style={'display': 'none'}),
        html.Div(children='size of plot data: {}'.format(g_df_all.shape[0]),
            id='plot-datasize', style={'margin-top': 10}),

        dcc.Graph(id='plot-3d',
            figure=g_fig
            )
        ])

app.layout = serve_layout

# definition of callbacks
@app.callback(
    Output(component_id='ndiv-value', component_property='children'),
    [Input(component_id='ndiv', component_property='value')]
)
def update_ndiv_value(_ndiv):
    return "ndiv value: {}".format(_ndiv)

@app.callback(
    Output(component_id='binmax-value', component_property='children'),
    [Input(component_id='binmax', component_property='value')]
)
def update_binmax_value(_binmax):
    return "binmax value: {}".format(_binmax)

# g_last_start_plot = None

#callback_state_list = g_ptable.make_state_list()
callback_state_list = g_ptable.make_state_list()
callback_state_list.append(State('selection-type', 'value'))
callback_state_list.append(State('selection-bool', 'value'))
callback_state_list.append(State('ndiv', 'value'))
callback_state_list.append(State('binmax', 'value'))
callback_state_list.append(State('start-plot-hidden', 'children'))

@app.callback(
    [Output(component_id='plot-3d', component_property='figure'),
     Output(component_id='plot-datasize', component_property='children'),
     Output('start-plot-hidden', 'children')],
    [Input(component_id='start-plot', component_property='n_clicks')],
    state = callback_state_list
)
def update_plot3d(start_plot, *args):
    arg_elements = list(args)[:-5]
    selected = g_ptable.convert_to_selected(*args)

    selection_type = list(args)[-5]
    selection_bool = list(args)[-4]
    if selection_bool == 'True':
        selection_bool = True
    else:
        selection_bool = False
    ndiv = list(args)[-3]
    binmax = list(args)[-2]
    last_plot = list(args)[-1]

    global g_df_all
    label = 'size of plot data: {}'.format(g_df_all.shape[0])
    if start_plot is None:
        # first expose
        print("first expose")
        trim = DfTrim(g_df_all, key=g_key_list, group=g_key_group_list, ndiv=ndiv, binmax=binmax)
        df_trim = trim.df
        label = 'size of plot data: {}'.format(df_trim.shape[0])
        print("new data size", df_trim.shape)
        fig = plot_go_3d(df_trim, g_key_list, g_key_display_list)
        return fig, label, last_plot
    if last_plot is None:
        doit = True
    elif start_plot > last_plot:
        doit = True
    else:
        doit = False
    if start_plot is not None:
        last_plot = start_plot
    if doit: 
        print("original data size", g_df_all.shape)
        if len(selected) == 0:
            df_select = g_df_all
        else:
            df_select = select_elements(g_df_all, selection_type, selection_bool, selected)
        print("selected data size", df_select.shape)

        trim = DfTrim(df_select, key=g_key_list, group=g_key_group_list, ndiv=ndiv, binmax=binmax)
        df_trim = trim.df
        label = 'size of plot data: {}'.format(df_trim.shape[0])
        print("df data size",df_trim.shape)
        fig = plot_go_3d(df_trim, g_key_list, g_key_display_list)
        return fig, label, last_plot
    else:
        raise ValueError("not doit, should not come here!")


# for name, button_id in zip(reversed(g_ptable.table_content_list), reversed(g_ptable.table_content_id_list)):
for name, button_id in zip(g_ptable.table_content_list, g_ptable.table_content_id_list):
    if name in g_hea4.elements_exist(g_df_all):
        @app.callback(Output(button_id, 'style'),
                      [Input(button_id, 'n_clicks')]
                      )
        def callback_onclick(n_clicks):
            if n_clicks is None:
                return g_ptable.normal_button
            if n_clicks % 2 == 0:
                return g_ptable.normal_button
            else:
                return g_ptable.clicked_button


if __name__ == "__main__":
    app.run_server(debug=False)
